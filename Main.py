'''
2017/10/06 By C.Y. Fang
'''

import youtube_dl
from PyQt4 import QtGui, QtCore

def my_hook(d):
    if d['status'] == 'finished':
        print('Done downloading, now converting ...')

def downloadYoutube(url):
    ydl_opts = {
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }],
        'logger': MyLogger(),
        'progress_hooks': [my_hook],
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([url])

class myThread(QtCore.QThread):
    def __init__(self,url,parent=None):
        super(self.__class__, self).__init__(parent)
        self.url = url

    def run(self):
        downloadYoutube(self.url)
        self.exit()

class MyLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)

class Window(QtGui.QWidget):
    def __init__(self, parent = None):
        super(Window, self).__init__(parent)
        self.setWindowTitle('Youtube Download')
        grid = QtGui.QGridLayout()
        self.btnDownload = QtGui.QPushButton("下載")
        self.btnDownload.clicked.connect(self.btnDownloadClicked)
        self.textbox = QtGui.QPlainTextEdit()
        self.textbox.resize(200,200)
        grid.addWidget(self.textbox,1,1)
        grid.addWidget(self.btnDownload,2,1)
        self.setLayout(grid)

    def btnDownloadClicked(self):
        urls = self.textbox.toPlainText().split('\n')
        if len(urls)>=1 and urls[0].strip():
            for u in urls:
                t = myThread(u)
                t.run()
        else:
            print("please input urls")


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    window = Window()
    window.show()
    sys.exit(app.exec_())
